## Authors
- [Pedro henrique romio dos santos](https://gitlab.com/pedrohenriqueromio)

# Teste Backend 01 - 

Desenvolvida uma taxonomia personalizada para posts (CPT) dentro do WordPress, para categorizar os posts por regiões. Sobreescrita de links permanentes dos arquivos de "Archive" e "Single" de posts por região.
Utilizado padrão MVC/Singleton.
Desenvolvido usando o tema base Twenty Twenty-Three

# Configurar o painel 
wp-admin/options-permalink.php
/%region%/%category%/%postname%/

# Como Forçar rewrite
flush_rewrite_rules();
add_action( 'init', 'flush_rewrite_rules' );

# Ex: Arquive e Single de notícias de São Paulo Capital
Archive: /sao-paulo/capital/noticias/
Single: /sao-paulo/capital/noticias/noticia-1/
Single: /sao-paulo/capital/noticias/noticia-2/

O path `/noticias/` refere-se a uma palavra estática no PATH,
ou uma categoria padrão de post (taxonomia category) que, idealmente,
deve também estar presente no PATH do post.

# Archive
/sao-paulo/capital é a categoria e subcategoria respectivamente 
/noticias é a categoria do post, caso seja atribuida alguma categoria ( category ) é usada a slug da categoria 

# Single
/sao-paulo/capital é a categoria e subcategoria respectivamente
noticia-1 é o slug da página 
Exemplo: /santa-catarina/lages/politica/post-03-rs-porto-alegre-2/

# Como filtrar Posts por região
Rota: /wp-json/wp/v2/posts?region={ID da região}

# Postman API
https://www.postman.com/pedrohenriqueromio/workspace/taopress/collection/9169316-d90ad40d-e74b-4267-aef6-c95e5dea53bf?ctx=documentation&tab=variables