<?php
/**
 * Region plugin Doc
 *
 * @category Region
 * @package   SeoxTest
 * @author    Pedro Henrique Romio Dos Santos <pedrohenriqueromio@gmail.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */

/**
 * TaoPress - Regiões para publicações
 * Plugin Name: TaoPress - Regiões para publicações 2
 * Plugin URI:  https://www.seox.com.br
 * Description: Habilita a configuração de Região para publicações, sendo possível atribuir as postagens e filtrar pela API.
 * Version:     1.0.0
 * Author:      Pedro Henrique Romio Dos Santos
 * Author URI:  https://www.linkedin.com/in/pedro-henrique-romio
 * Text Domain: plugin-slug
 * License:     GPL-2.0-or-later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

require __DIR__ . '/vendor/autoload.php';

Seox\TesteBackend\Region::get_instance();
