<?php
/**
 * Region manager plugin
 *
 * Desenvolvida uma taxonomia personalizada para posts (CPT) dentro do WordPress, para categorizar os posts por regiões. Sobreescrita de links permanentes dos arquivos de "Archive" e "Single" de posts por região.Utilizado padrão MVC/Singleton.Desenvolvido usando o tema base Twenty Twenty-Three
 *
 * PHP version 7.4
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    SeoxTest
 * @author     Pedro Henrique Romio Dos Santos <pedrohenriqueromio@gmail.com>
 * @copyright  1997-2005 The PHP Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @since      File available since Release 1.0.0
 */

namespace Seox\TesteBackend;

/**
 * Region Class plugin
 *
 * Region Class Plugin, custom taxonomy, rewrite route and API custom endpoints
 *
 * @package  Region
 * @author   Author <pedrohenriqueromio@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://localhost/
 */
class Region {

	/**
	 * The unique instance of the plugin.
	 *
	 * @var Region
	 */
	private static $instance;

	/**
	 * Gets an instance of our plugin.
	 *
	 * @return Region
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Constructor.
	 */
	private function __construct() {
		// hook into the init action and call create_region_hierarchical_taxonomy when it fires!
		add_action( 'init', array( &$this, 'create_region_hierarchical_taxonomy' ), 0 );
		// register links to be rewrite!
		add_action( 'init', array( &$this, 'region_post_type_rewrites' ) );

		// Filters to post link!
		add_filter( 'post_link', array( &$this, 'region_filter_post_type_link' ), 10, 2 );
		// add the filter!
		add_filter( 'rest_post_query', array( &$this, 'filter_rest_post_query' ), 10, 2 );

	}

	/**
	 * Create a custom taxonomy name it "Região" for your posts!
	 */
	public function create_region_hierarchical_taxonomy() {

		flush_rewrite_rules();
		add_action( 'init', 'flush_rewrite_rules' );

		$labels = array(
			'name'              => _x( 'Região', 'Região' ),
			'singular_name'     => _x( 'Região', 'Região' ),
			'search_items'      => __( 'Procurar por Região' ),
			'all_items'         => __( 'Todas Regiões' ),
			'parent_item'       => __( 'Região Ascendente' ),
			'parent_item_colon' => __( 'Ascendente Região:' ),
			'edit_item'         => __( 'Editar Região' ),
			'update_item'       => __( 'Atualizar Região' ),
			'add_new_item'      => __( 'Adicionar nova Região' ),
			'new_item_name'     => __( 'Nova Região' ),
			'menu_name'         => __( 'Região' ),
		);

		// Now register the taxonomy!
		register_taxonomy(
			'region',
			array( 'post' ),
			array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array(
					'slug'         => '/',
					'with_front'   => false,
					'hierarchical' => true,
				),
			)
		);
	}

	/**
	 * Return posts enabled to use this taxonomy
	 *
	 * @return array<string>
	 */
	public function region_get_post_types() {
		return array(
			'post',
		);
	}

	/**
	 * Filter to create dinamic permalinks
	 *
	 * @param mixed $post_link link of the post.
	 * @param mixed $post current post.
	 * @return mixed
	 */
	public function region_filter_post_type_link( $post_link, $post ) {

		$taxonomy = 'region';
		$terms    = get_the_terms( get_the_ID(), $taxonomy );

		$slug = array();
		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				if ( 0 === $term->parent ) {
					array_unshift( $slug, sanitize_title_with_dashes( $term->name ) );
				} else {
					array_push( $slug, sanitize_title_with_dashes( $term->name ) );
				};
			};

			if ( ! empty( $slug ) ) {
				$post_link = str_replace( '%' . $taxonomy . '%', join( '/', $slug ), $post_link );
			}
		} else {
			$post_link = str_replace( '%' . $taxonomy . '%/', '', $post_link );
		}
		return $post_link;
	}

	/**
	 * Set the appropriate rewrite rules
	 */
	public function region_post_type_rewrites() {

		$post_types = '';
		foreach ( $this->region_get_post_types() as $ptype ) {
			$post_types .= '&post_type[]=' . $ptype;
		}

		$terms = get_terms(
			array(
				'taxonomy'   => 'region',
				'hide_empty' => false,
			)
		);

		$category = get_terms(
			array(
				'taxonomy'   => 'category',
				'hide_empty' => false,
			)
		);

		foreach ( $terms as $term ) {
			if ( 0 !== $term->parent ) {
				foreach ( $category as $cat ) {
					$parent = get_term_by( 'term_id', $term->parent, 'region' );
					// create a custom permalink: state/city/category/postname !
					add_rewrite_rule( $parent->slug . '/' . $term->slug . '/' . $cat->slug . '/([^/]+)(/[0-9]+)?/?$', 'index.php?&name=$matches[1]' . $post_types, 'top' );

					// create a custom permalink: state/city/category/ !
					add_rewrite_rule( $parent->slug . '/([^/]+)(/[0-9]+)?/' . $cat->slug, 'index.php?region=$matches[1]', 'top' );
				}
				// create a custom permalink: state/category/postname !
				add_rewrite_rule( $parent->slug . '/' . $cat->slug . '/([^/]+)(/[0-9]+)?', 'index.php?region=$matches[1]', 'top' );
			}
		}

		if ( ! empty( $category ) ) {
			foreach ( $category as $term ) {
				// create a custom permalink: category/postname!
				add_rewrite_rule( $term->slug . '/([^/]+)(/[0-9]+)?/?$', 'index.php?&name=$matches[1]' . $post_types, 'top' );
			}
		}
	}

	/**
	 * Create a custom API Rest route
	 * wp-json/wp/v2/posts?region={id}
	 *
	 * @param mixed $args args params.
	 * @param mixed $request request params.
	 * @return mixed
	 */
	public function filter_rest_post_query( $args, $request ) {
		$params = $request->get_params();
		if ( isset( $params['region'] ) ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'region',
					'field'    => 'term_id',
					'terms'    => $params['region'],
				),
			);
		}
		return $args;
	}

}

